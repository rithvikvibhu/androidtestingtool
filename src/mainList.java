import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashSet;
import java.util.Iterator;


//This class reads the file that contains the main API list//
public class mainList {
	
	BufferedReader br;
	String fileContent;
	HashSet<String> MainSet;
	
	
	
	
	public mainList()
	{
		
		MainSet = new HashSet<String>();
		try {
			 
			String sCurrentLine;
			File file = new File("mainList.txt");
			br = new BufferedReader(new FileReader(file));
			
			//System.out.println(file.getAbsolutePath());
				
			StringBuilder strBld = new StringBuilder();
			while ((sCurrentLine = br.readLine()) != null) 
			{
				
				MainSet.add(sCurrentLine);
					
			}
			
 
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		
		finally 
		{
			try {
				if (br != null)br.close();
			    } 
			catch (IOException ex) 
			{
				ex.printStackTrace();
			}
	    }
	}


//This method is not used//
	public boolean contains(String sCurrentLine) //This method compares each of the two files, line by line and produces the common methods// 
	{
		// TODO Auto-generated method stub
		
		for (Iterator<String> iterator = MainSet.iterator(); iterator.hasNext();) {
			String tmp = iterator.next();
			if(tmp.equals(sCurrentLine))//Comparison taking place//
				
			return true;
			
		}
		return false;
	}
	
	
}