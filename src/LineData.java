import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.StringTokenizer;

//This class is used to display the API's with complete method signature and class path//
public class LineData {
	
	String pattern; //stands for complete method signature//
	String path; //stands for the complete path name to which the method belongs to//
	HashSet<String> line = new  HashSet();
	
	public LineData(String pattern, String path){
		File file = new File(path);
		System.out.println(pattern+":"+path);
		
		try {
			
			FileInputStream fis = new FileInputStream(file);
			int i;
			String content = "";
			while((i=fis.read()) != -1)
				content = content + (char)i;
			
			StringTokenizer sT = new StringTokenizer(content,"\n");
			
			while(sT.hasMoreTokens()){
				
				String tmp = sT.nextToken();
				
				if(hasPattern(tmp)){
					line.add(tmp);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

	public HashSet<String> getLines(){
		return line;
	}
	
	public String getPattern(){
		return this.pattern;
	}

	public String getPath(){
		return this.path;
	}
	private boolean hasPattern(String tmp) {
		
		StringBuffer bPattern = new StringBuffer(pattern);
		StringBuffer bTemp = new StringBuffer(tmp);
		
		for(int i=0;bPattern.length() >= bTemp.length();i++){
			
			if(bPattern.toString().startsWith(bTemp.toString()))
				return true;
			
			else
				bPattern = new StringBuffer(bPattern.substring(i));	
		}
		
		return false;
	}

}
