import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.StringTokenizer;


public class APIData {
	
	File file = new File("api.txt");
	BufferedReader br;
	String fromPackage;
	String codeLine;
	String type;
	String pattern;
	public APIData(String pattern){
		
		this.pattern =  pattern;
		try {
			br = new BufferedReader(new FileReader(file));
			String currentLine;
			while((currentLine = br.readLine())!=null){
				
				if(currentLine.startsWith("class")){
					StringTokenizer st = new StringTokenizer(currentLine," ");
					st.nextToken();
					fromPackage = st.nextToken();
					continue;
				}
				
				StringBuffer strBuf = new StringBuffer(currentLine);
				
				boolean flag = true;
				
				for(int i=0;i<(currentLine.length()-pattern.length());i++){
					if(strBuf.substring(i).startsWith(pattern)){
						StringTokenizer str = new StringTokenizer(currentLine,"~");
						//System.out.println(currentLine);
						if(str.hasMoreTokens())
						codeLine=str.nextToken();
						else
							codeLine=currentLine;
						
						if(str.hasMoreTokens())
							type=str.nextToken();
						
						flag = false;
						
						break;
					}
					
				if(!flag)
					break;
				}
			}
			
			if(codeLine==null)
				fromPackage=null;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	} 
	

}
