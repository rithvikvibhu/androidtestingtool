import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashSet;
import java.util.StringTokenizer;


//This class reads a file and generates the set// // Reads the java source file//
public class FetchClass 
{
	
	BufferedReader br;
	String fileContent;
	HashSet<String> set;
	private mainList mL;
	private StringTokenizer st;
	String path;
	
	public FetchClass(String classPath)
	{

		set = new HashSet<String>();
		mL = new mainList();
		this.path = classPath;
			ExtractTokens eT;
			try 
			{
				eT = new ExtractTokens(classPath);
				eT.extract();
				
				fileContent = eT.FileContent; 
				
				st = new StringTokenizer(fileContent,"\n");	
				
				
				
				while(st.hasMoreTokens())
				{
					 ;//To store the line of file//
					String tmp = st.nextToken();
					if(!tmp.equals("\n"))
					{
						set.add(tmp);
					}
				}
				
				
				
			} catch (IOException e){
				e.printStackTrace();
			}
	}
}
	