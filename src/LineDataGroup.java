import java.util.HashSet;


public class LineDataGroup {
	
	public static HashSet<LineData> Results = new HashSet<LineData>();
	public static HashSet<String> paths = new HashSet<String>();
	public static HashSet<String> paterns = new HashSet<String>();
	
	public static void updateResults(){
		for(String path : paths){
			for(String patern : paterns  ){
				Results.add(new LineData(patern,path));
			}
		}
	}
}
