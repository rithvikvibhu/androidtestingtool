//This method is used to get the list of files from the directories//
import java.awt.List;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;


public class ListFileGroup 
{
	
	File root;
	public HashSet<File> fileLists = new HashSet<File>();
	
	public ListFileGroup(String rootPath)
	{

		updateList(rootPath);
	}
	
	public void updateList(String path)
	{
		
		File parent = new File(path);
		File[] sub = parent.listFiles();
	
		
		for(File f : sub)
		{
			if(f.isDirectory())
			updateList(f.getPath());
			
			else
			{
					if(f.getName().endsWith("java"))
					//System.out.println(f);
					fileLists.add(f);
					//System.out.println(f.getAbsolutePath());
			}
				
		}
	}		
}