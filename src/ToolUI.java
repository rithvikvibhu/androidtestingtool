import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashSet;
import java.util.Vector;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class ToolUI extends JFrame implements ActionListener,ListSelectionListener
{

	//JFileChooser jFC = new JFileChooser();
	JTextField inputField = new JTextField();//This text field is used to enter the class path of the files//
	
	JButton browse = new JButton("browse");//Used to browse for the files//
	
	JPanel inputPanel = new JPanel();
	
	JTextArea output = new JTextArea();//Used to display the final output//
	
    JButton check = new JButton("Check");//This button is used to start the operation of intersection//
    
    JPanel outputPanel = new JPanel();
    
    //String str[] = {"akash","v","hoysal"} ;
    
    JList listbox = new JList();
    
    JPanel labelPanel = new JPanel();
    
    DefaultListModel lm = new DefaultListModel();    
    
    JLabel meathod = new JLabel("Meathod:");  
    JLabel packageN = new JLabel("Package:");
    JLabel type    = new JLabel("Type:");
    JLabel meathodName = new JLabel("         ");  
    JLabel packageName = new JLabel("         ");
    JLabel typeName    = new JLabel("         ");
    
    JPanel inputPanel1 = new JPanel();
	
    
    Vector<APIData> outputData = new Vector<APIData>();
    
    public ToolUI()
	{
    	meathod.setPreferredSize(new Dimension(100,40));
    	packageN.setPreferredSize(new Dimension(100,40));
    	type.setPreferredSize(new Dimension(100,40));
    	inputPanel.setPreferredSize(new Dimension(400,30));
    	setLayout(new FlowLayout());
		//inputPanel.setLayout(new FlowLayout());
		inputField.setPreferredSize(new Dimension(200,20));
		//check.setPreferredSize(new Dimension(100,20));
		inputPanel.add(inputField);//Adding the elements to the panel// 
		inputPanel.add(browse);
		
		listbox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		outputPanel.setLayout(new GridLayout(0,2));
		
		browse.addActionListener(this);
		check.addActionListener(this);
		inputPanel1.setPreferredSize(new Dimension(400,50));
		setSize(640,240); 
		
		inputPanel.add(check);
		inputPanel1.add(inputPanel);
		//inputPanel.add(output);
		add(inputPanel1);
		
		labelPanel.setLayout(new GridLayout(0,2));
		
		labelPanel.add(meathod,SpringLayout.WEST);
		labelPanel.add(meathodName,SpringLayout.EAST);
		labelPanel.add(type,SpringLayout.WEST);
		labelPanel.add(typeName,SpringLayout.EAST);
		labelPanel.add(packageN,SpringLayout.WEST);
		labelPanel.add(packageName,SpringLayout.EAST);
		
		listbox.setBorder(BorderFactory.createEtchedBorder(1));
		outputPanel.add(listbox);
		outputPanel.add(labelPanel);
		add(outputPanel);
		setVisible(true);
		
		listbox.addListSelectionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		// TODO Auto-generated method stub
		if(e.getSource()==browse)//If the browse button is pressed//
		{
			
		}
		if(e.getSource()==check)//If the check button is pressed, then the intersection is calculated//
		{
			compute();
		}

	}
	
	public void compute()
	{
		System.out.println("Computing");
	//	HashSet<String> unionAll = new HashSet<String>(); 
		HashSet<FetchClass> input = new HashSet<FetchClass>();
		HashSet<String> outputSet = new HashSet<String>();
			
		mainList ml = new mainList();//Making an object of the type mainList//
			
			ListFileGroup lFG = new ListFileGroup(inputField.getText());
			
			for(File f : lFG.fileLists)
			{
				input.add(new FetchClass(f.getAbsolutePath()));//Sending the class path to FetchClass to get a list of all the files//
			}
			
			for(FetchClass fC : input)
			{
				//change this to union operation
				fC.set.retainAll(ml.MainSet);
				outputSet.addAll(fC.set);
			}
			
			System.out.println(outputSet);
			
			for(String str : outputSet){
				outputData.add(new APIData(str));
				
			}
			
			for(APIData a : outputData){
				System.out.println(a.codeLine+" form package :" + a.fromPackage);
				lm.addElement(a.pattern);
				listbox.setModel(lm);
			}
			
			
			
		}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getValueIsAdjusting()){
			
			int index =	listbox.getSelectedIndex();
			
			APIData ad = outputData.get(index);
		
			this.meathodName.setText(ad.codeLine);
			this.typeName.setText(ad.type);
			this.packageName.setText(ad.fromPackage);
		}
	}
}
